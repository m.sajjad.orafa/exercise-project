import 'package:flutter/material.dart';

class customeGridList extends StatelessWidget {
  const customeGridList({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      crossAxisCount: 2,
      children: List.generate(10, (index) {
        return Column(
          children: [
            Icon(Icons.access_alarm_outlined),
            Text(
              'Item $index',
              style: Theme.of(context).textTheme.headline5,
            ),
          ],
        );
      }),
    );
  }
}