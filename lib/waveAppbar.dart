import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wave/config.dart';
import 'package:wave/wave.dart';

class waveAppBar extends StatefulWidget implements PreferredSizeWidget {
  waveAppBar({Key key})
      : preferredSize = Size.fromHeight(180),
        super(key: key);

  @override
  final Size preferredSize;

  @override
  _CustomAppBarState createState() => _CustomAppBarState();
}

class _CustomAppBarState extends State<waveAppBar> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Container(
      color: Theme.of(context).primaryColor,
      width: MediaQuery.of(context).size.width,
      // Set Appbar wave height
      child: Container(
        height: 180,
        color: Theme.of(context).scaffoldBackgroundColor,
        child: Container(
            color: Colors.white,
            child: Stack(
              children: <Widget>[
                RotatedBox(
                    quarterTurns: 2,
                    child: WaveWidget(
                      config: CustomConfig(
                        colors: [Theme.of(context).primaryColor],
                        durations: [22000],
                        heightPercentages: [-0.1],
                      ),
                      size: Size(double.infinity, double.infinity),
                      waveAmplitude: 1,
                    )),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Center(
                        child: Text(
                      "عنوان صفحه",
                      style: TextStyle(fontSize: 20, color: Colors.white),
                    )),
                    Padding(
                      padding: const EdgeInsets.only(left: 40,right: 40,top: 10),
                      child: TextField(
                        onChanged: (value) {},
                        decoration: InputDecoration(
                            labelText: "جست و جو",
                            hintText: "جست و جو",
                            prefixIcon: Icon(Icons.search),
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(25.0)))),
                      ),
                    ),
                  ],
                ),
              ],
            )),
      ),
    ));
  }
}
